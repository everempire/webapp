// @flow
import React, { useEffect, useState } from 'react';
import styled from 'styled-components';

import { Rest } from 'rest';

const Style = styled.div`
  select {
    display: block;
    margin: 8px 0;
  }

  label {
    display: inline-block;
    width: 90px;
    margin-bottom: 8px;
  }
`;

export const SignIn = () => {
  const [users, setUsers] = useState([]);
  const [selectedUser, setSelectedUser] = useState('');

  const [email, setEmail] = useState('GameDevFox@gmail.com');
  const [name, setName] = useState('GameDevFox');

  useEffect(() => {
    Rest.listUsers().then(users => {
      users[0] && setSelectedUser(users[0].email);
      setUsers(users);
    });
  }, []);

  const [result, setResult] = useState();

  const onSignInClick = () => {
    Rest.auth({ email: selectedUser }).then(token => {
      Rest.setToken(token);
    });
  };

  const onRegisterClick = () => {
    Rest.register({ email, name }).then(result => {
      setResult(result);
    });
  };

  return (
    <Style>
      <div className="auth">
        <h2>Sign In</h2>

        {JSON.stringify(selectedUser, null, 2)}
        <select value={selectedUser} onChange={e => setSelectedUser(e.currentTarget.value)}>
          {users.map(user => {
            return (<option key={user.id} value={user.email}>{user.name} ({user.email})</option>);
          })}
        </select>

        <button type="button" onClick={onSignInClick}>Sign In</button>
      </div>

      <hr/>

      <div className="register">
        <h2>Register</h2>

        {result && <pre>{result}</pre>}

        <div>
          <label>E-mail:</label>
          <input type="text" value={email} onChange={e => setEmail(e.currentTarget.value)}/>
        </div>

        <div>
          <label>Player Name:</label>
          <input type="text" value={name} onChange={e => setName(e.currentTarget.value)}/>
        </div>

        <button type="button" onClick={onRegisterClick}>Register</button>
      </div>
    </Style>
  );
};
