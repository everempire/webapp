export const Recycle = () => {
  let counter = 0;

  let recycle = [];
  let sorted = true;

  const take = () => {
    if(recycle.length) {
      if(!sorted) {
        recycle.sort((a, b) => a - b);
        sorted = true;
      }

      return recycle.shift();
    }

    return counter++;
  };

  const give = id => {
    recycle.push(id);

    sorted = false;
    return id;
  };

  const reset = () => {
    counter = 0;
    recycle = [];
  };

  return { take, give, reset };
};
