import styled from 'styled-components';

export const lightGrey = '#dddddd';
export const lightRed = '#ffdddd';
export const lightOrange = '#ffeede';
export const lightYellow = '#ffffdd';
export const lightGreen = '#ddffdd';
export const lightBlue = '#ddddff';
export const lightPurple = '#eedeff';

export const Style = styled.div`
  display: inline-block;
  position: relative;

  cursor: pointer;

  border: 2px solid #888;
  border-radius: 4px;

  &.value {
    background-color: ${lightGrey};
  }

  &.node {
    background-color: ${lightYellow};
  }

  &.action, &.dispatch {
    background-color: ${lightRed};
  }

  &.selected {
    border-color: red;
  }

  &.selected {
    border-color: red;
  }

  & > .tag {
    position: absolute;

    top: 0;
    right: 0;

    color: #888;
    background-color: black;
    border-radius: 4px;

    padding: 1px 4px;

    font-family: monospace;
    font-weight: bold;

    &.starts-with {
      color: white;
    }

    &.matches .match {
      color: yellow;
    }

    .match {
      position: absolute;
      color: #88f;
    }
  }
`;
