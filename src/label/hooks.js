import { useContext, useEffect, useState } from 'react';

import { Context } from './context';

const enter = (context, labelId) => {
  const {
    labels, labelIdTagMap,
  } = context;

  const label = labels[labelId];
  const tag = labelIdTagMap[labelId];
  label.setTag(tag);
};

const exit = (context, labelId) => {
  const { labels } = context;

  const label = labels[labelId];
  label.setTag(undefined);
};

let labelCount = 0;

export const useLabel = value => {
  const context = useContext(Context);

  const [tag, setTag] = useState();
  const [labelId] = useState(() => labelCount++);

  // TODO: Consider using useLayoutEffect instead
  useEffect(() => {
    const {
      labels, tagIds,
      labelIdTagIdMap, labelIdTagMap, tagLabelIdMap,
      markFn, labelFn,
    } = context;

    // Register
    const label = { value, setTag };
    labels[labelId] = label;

    // Generate tag
    const tagId = tagIds.take();
    const tag = labelFn(tagId);

    labelIdTagIdMap[labelId] = tagId;
    labelIdTagMap[labelId] = tag;

    tagLabelIdMap[tag] = labelId;

    // Enter if valid
    if(markFn(value))
      enter(context, labelId);

    return () => {
      if(markFn(value))
        exit(context, labelId);

      tagIds.give(tagId);

      delete labels[labelId];
      delete labelIdTagIdMap[labelId];
      delete labelIdTagMap[labelId];

      const tag = labelIdTagMap[labelId];
      delete tagLabelIdMap[tag];
    };
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [value]);

  return tag;
};

export const useLabelControls = () => {
  const context = useContext(Context);

  const mark = (markFn = () => true) => {
    const { labels, labelIdTagMap } = context;
    context.markFn = markFn;

    // Clear old tags
    Object.keys(labelIdTagMap).forEach(labelId => {
      const label = labels[labelId];
      if(label === undefined) {
        console.warn('Undefined label');
        return;
      }

      const { value } = label;

      if(!markFn(value))
        exit(context, labelId);
    });

    // Set new tags
    Object.entries(labels).forEach(([labelId, label]) => {
      // TODO: Can we filter out some labels or iterate
      // over a mode ideal set?

      // if(labelId in labelIdTagIdMap)
      //   return;

      const { value } = label;

      if(markFn(value))
        enter(context, labelId);
    });
  };

  const select = tag => {
    const { labels, tagLabelIdMap } = context;

    if(!(tag in tagLabelIdMap))
      return undefined;

    const labelId = tagLabelIdMap[tag];
    if(!(labelId in labels))
      return undefined;

    const label = labels[labelId];
    return label.value;
  };

  const clear = () => mark(() => false);

  return { mark, clear, select };
};
