import classNames from 'classnames';
import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';

import { executeSelection } from 'console/select';
import { useLabel } from 'app/label';

import { Style } from './label.style';

const typeOrder = [
  'action',
  'dispatch',
  'node',
];

export const typeOf = value => {
  if(!value)
    return null;

  return typeOrder.find(type => type in value) || 'value';
};

export const LabelComponent = props => {
  const {
    label: stateLabel, activeNode, className, children, value,
    dispatch,
  } = props;

  const highlight = stateLabel || '';

  const label = useLabel(value);

  const startsWith = label && label.startsWith(highlight);
  const matches = label && label === highlight;

  const tagClasses = { 'starts-with': startsWith, matches };

  const labelBox = label && (
    <div className={classNames('tag', tagClasses)}>
      {startsWith && <span className="match">{highlight}</span>}
      <span className="base">{label}</span>
    </div>
  );

  const labelType = typeOf(value);
  const selected = value && value.node && value.node === activeNode;

  const onClick = e => {
    e.stopPropagation();
    executeSelection({ selection: value, dispatch });
  };

  return (
    <Style
      className={classNames('label', labelType, { selected }, className)}
      onClick={onClick}
    >
      <div className="children">{children}</div>
      {labelBox}
    </Style>
  );
};

export const Component = LabelComponent;

export const Label = connect(state => {
  const { label, activeNode } = state.console.nodes;
  return { label, activeNode };
})(LabelComponent);

export const SimpleLabel = styled(Label)`
  & {
    font-weight: bold;
    padding: 0 4px;
  }
`;
