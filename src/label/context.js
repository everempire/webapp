import React, { createContext } from 'react';

import { Recycle } from './recycle';

export const Context = createContext();

export const Provider = props => {
  const { children, onLabel } = props;

  const labelContext = {
    labels: {},
    tagLabelIdMap: {},
    labelIdTagMap: {},

    tagIds: Recycle(),
    labelIdTagIdMap: {},

    tagTagIdMap: {},

    markFn: () => false,
    labelFn: onLabel || (x => x),
  };

  return (
    <Context.Provider value={labelContext}>
      {children}
    </Context.Provider>
  );
};
