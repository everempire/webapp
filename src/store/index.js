import { combineReducers, createStore } from 'redux';

import { reducer as consoleReducer } from 'console/store';

import { Reducer } from './redux-utils';

const prodCreateStore = reducer => createStore(reducer);

const devCreateStore = reducer => {
  const enhancer = window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__();
  return createStore(reducer, enhancer);
};

const isDev = process.env.NODE_ENV.toLowerCase() === 'development';
const envCreateStore = isDev ? devCreateStore : prodCreateStore;

const sampleReducer = Reducer({
  MY_ACTION: (state, action) => {
    const { increment } = action;
    return { ...state, count: state.count + increment };
  },
}, {
  name: 'Some App',
  count: 123,
});

const reducer = combineReducers({
  sample: sampleReducer,
  console: consoleReducer,
});

export const store = envCreateStore(reducer);
