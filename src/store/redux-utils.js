export const Action = type => {
  return args => ({ ...args, type });
};

export const Reducer = (handlers, initialState) => {
  return (oldState, action) => {
    if(oldState === undefined)
      return initialState;

    const { type } = action;
    const actionHandler = handlers[type];

    let state = oldState;
    if(actionHandler)
      state = actionHandler(oldState, action);

    return state;
  };
};

export const bindActions = (actions, dispatch) => {
  const boundActions = {};
  Object.entries(actions).forEach(([name, fn]) => {
    boundActions[name] = (...args) => dispatch(fn(...args));
  });

  return boundActions;
};
