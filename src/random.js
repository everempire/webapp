// TODO: Reserch implications of using and merge into kitsune/commons
import { randomBytes } from 'crypto';

const BYTES = 256 / 8; // 256 bits

export const random = () => randomBytes(BYTES).toString('base64');
