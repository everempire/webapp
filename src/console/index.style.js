import styled from 'styled-components';

import { Style as Node } from 'app/node';

export const Style = styled.div`
  display: flex;
  flex-direction: column;

  padding: 4px;
  height: 100%;

  & > .action-nodes {
    display: flex;
    justify-content: space-between;
    margin: 4px 0;

    .label {
      margin-right: 4px;
    }
  }

  & > .split {
    flex-grow: 1;

    display: flex;
    padding-top: 4px;

    & > .control {
      border-right: 4px solid black;
      padding-right: 8px;
      margin-right: 8px;
    }

    & > .view-box {
      flex-grow: 1;

      position: relative;

      & > .view {
        position: absolute;
        height: 100%;
        width: 100%;
        overflow-y: auto;
      }
    }
  }

  & > .actions {
    position: fixed;
    bottom: 0;
    left: 0;

    width: 100%;
  }

  // Node styles
  ${Node}.null {
    color: red;
    font-style: italic;
    font-weight: bold;
  }

  ${Node}.boolean, ${Node}.number {
    font-weight: bold;
    text-decoration: underline;
  }

  ${Node}.string {
    color: green;
    font-weight: bold;
  }

  ${Node}.name {
    color: blue;
    font-weight: bold;
  }
`;
