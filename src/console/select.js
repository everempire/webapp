import { typeOf } from 'app/label';

import { actions as actionMap } from './input/actions';
import { setNodes, setActiveNode, setFromActiveNode } from './store/actions';

const modes = {
  // Left
  node: ({ selection, dispatch }) => {
    const { node } = selection;
    dispatch(setNodes({ activeNode: node }));
  },
  // Right
  setNode: ({ selection, dispatch }) => {
    const { node } = selection;
    dispatch(setFromActiveNode(node));
  },
  // Down
  value: ({ selection, dispatch }) => {
    const { value } = selection;
    dispatch(setActiveNode(value));
  },

  action: args => {
    const { selection } = args;
    const { action } = selection;
    const id = typeof action === 'object' ? action.id : action;

    const fn = actionMap[id];
    if(!fn)
      throw new Error(`No action for name: ${id}`);

    const newArgs = { ...args };
    if(typeof action === 'object' && 'input' in action)
      newArgs.input = action.input;

    fn(newArgs);
  },

  dispatch: ({ selection, dispatch }) => {
    const { dispatch: action } = selection;
    dispatch(action);
  },
};

export const executeSelection = ({ selection, mode, dispatch, args = {} }) => {
  if(selection === undefined)
    dispatch(setActiveNode(undefined));
  else if(mode in modes)
    modes[mode]({ ...args, selection, dispatch });
  else {
    // Auto mode
    const type = typeOf(selection);
    if(type in modes)
      modes[type]({ ...args, selection, dispatch });
    else {
      const { value } = selection;
      dispatch(setActiveNode(value));
    }
  }
};
