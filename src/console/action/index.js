import { Rest } from 'rest';

import { setNodes } from '../store/actions';

const { writeEdge, writeString, writeMap, addName } = Rest.console;

export const INCOMPLETE = 'INCOMPLETE';
export const RUNNING = 'RUNNING';
export const COMPLETE = 'COMPLETE';

export const actions = {
  writeEdge: ({ input, dispatch }) => {
    const { head, tail } = input;

    return writeEdge({ head, tail })
      .then(id => {
        dispatch(setNodes({ id }));
        return { id };
      });
  },

  writeString: ({ input, dispatch }) => {
    const { string } = input;

    return writeString(string)
      .then(id => {
        dispatch(setNodes({ id }));
        return { id };
      });
  },

  writeMap: ({ input, dispatch }) => {
    return writeMap(input)
      .then(id => {
        dispatch(setNodes({ id }));
        return { id };
      });
  },

  name: ({ input }) => {
    const { node, nameId } = input;
    return addName(node, nameId).then(id => ({ id }));
  },
};
