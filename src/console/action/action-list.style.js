import styled from 'styled-components';

export const Style = styled.div`
  & > * {
    display: block;
    padding: 4px;
    margin: 0 4px 4px 4px;

    & > .tag {
      left: 0;
      right: initial;
    }

    .close {
      position: absolute;
      top: 4px;
      right: 4px;
    }

    .header {
      margin-bottom: 4px;

      & > .type, & > .status {
        font-weight: bold;
      }
    }

    .status {
      color: white;
      background-color: black;
      border-radius: 4px;
      padding: 0px 3px;
      margin-left: 4px;
    }

    .input, .output {
      & > .node {
        margin-right: 4px;
        margin-bottom: 4px;

        & > .key {
          margin-right: 4px;
        }
      }
    }
  }
`;
