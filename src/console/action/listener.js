import { useEffect } from 'react';
import { connect } from 'react-redux';

import { setNodes, updateAction } from '../store/actions';

import { actions as nodeActions, INCOMPLETE, RUNNING, COMPLETE } from '.';

export const Component = props => {
  useEffect(() => {
    const { actions, nodes, dispatch } = props;

    // Run ready actions
    const ready = actions
      .filter(action => action.status === INCOMPLETE)
      .filter(action => Object.values(action.input).every(input => nodes[input] !== undefined));

    ready.forEach(action => {
      const { id, type, input, output } = action;
      dispatch(updateAction(id, { status: RUNNING }));

      const inputValues = {};
      Object.entries(input).forEach(([name, node]) => {
        inputValues[name] = nodes[node];
      });

      nodeActions[type]({ input: inputValues, dispatch }).then(result => {
        Object.entries(result).forEach(([key, value]) => {
          const node = output[key];
          dispatch(setNodes({ [node]: value }));
        });

        dispatch(updateAction(id, { status: COMPLETE }));
      });
    });
  });

  return null;
};

export const ActionListener = connect(state => {
  const { actions, nodes } = state.console;
  return { actions, nodes };
})(Component);
