import { INCOMPLETE } from '.';

import { random } from '../../random';

import { setNode } from '../store/util';

const actionData = {
  random: {
    input: [],
    output: ['id'],
  },
  writeEdge: {
    input: ['head', 'tail'],
    output: ['id'],
  },
  writeString: {
    input: ['string'],
    output: ['id'],
  },
  writeMap: {
    input: [],
    output: ['id'],
  },
  name: {
    input: ['node', 'nameId'],
    output: ['id'],
  },
};

export const reducers = {
  ADD_ACTION: (state, { actionId, focus, initial }) => {
    const { actions } = state;

    const input = {};
    const output = {};

    const data = actionData[actionId];
    data.input.forEach(name => (input[name] = random()));
    data.output.forEach(name => (output[name] = random()));

    if(focus in input)
      state = setNode(state, 'activeNode', input[focus]);

    if(initial) {
      Object.entries(initial).forEach(([key, value]) => {
        if(!(key in input))
          input[key] = random();

        const id = input[key];
        state = setNode(state, id, value);
      });
    }

    const newAction = { id: random(), type: actionId, status: INCOMPLETE, input, output };
    state = { ...state, actions: [...actions, newAction] };

    return state;
  },

  UPDATE_ACTION: (state, { actionId, update }) => {
    const { actions } = state;

    const newActions = actions.map(action => {
      const { id } = action;
      if(id !== actionId)
        return action;

      return { ...action, ...update };
    });

    return { ...state, actions: newActions };
  },

  REMOVE_ACTION: (state, { actionId }) => {
    const actions = state.actions.filter(action => {
      const match = action.id === actionId;
      if(match) {
        // Clear random action nodes from `nodes`
        const { input, output } = action;
        Object.values(output).forEach(node => (state = setNode(state, node, undefined)));
        Object.values(input).forEach(node => (state = setNode(state, node, undefined)));
      }

      return !match;
    });
    return { ...state, actions };
  },
};
