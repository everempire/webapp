import React from 'react';
import { connect } from 'react-redux';

import { removeAction } from '../store/actions';

import { Close } from 'app/close';
import { Label } from 'app/label';
import { Node } from 'app/node';

import { INCOMPLETE } from '.';

import { Style } from './action-list.style';

export const Component = props => {
  const { actions, nodes } = props;

  const ioNodes = (value, input = true) => Object.entries(value).map(([name, node]) => {
    const labelValue = { value: nodes[node] };
    if(input)
      labelValue.node = node;

    return (
      <div key={name} className="node">
        <Label className="key" value={{ value: name }}><Node value={name}/></Label>
        <Label className="value" value={labelValue}><Node value={nodes[node]}/></Label>
      </div>
    );
  });

  return (
    <Style className="actions">
      {actions.map(action => {
        const { id, type, status, input, output } = action;

        return (
          <Label key={id} value={{ value: id }}>
            <Close value={{ dispatch: removeAction(id) }}/>

            <div className="header">
              <span className="type">{type}</span>
              <span className="status">{status}</span>
            </div>

            <div className="input">
              {ioNodes(input, status === INCOMPLETE)}
            </div>

            <hr/>

            <div className="output">
              {ioNodes(output, false)}
            </div>
          </Label>
        );
      })}
    </Style>
  );
};

export const ActionList = connect(state => state.console)(Component);
