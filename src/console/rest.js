const encode = encodeURIComponent;

export const endpoints = rest => ({
  // Edge
  writeEdge: ({ head, tail }) => rest.put('/edge', { head, tail }),
  destroyEdge: id => rest.delete(`/edge/${encode(id)}`),
  readEdge: id => rest.get(`/node/${encode(id)}/edge`),
  listEdges: () => rest.get('/edge'),

  // String
  listStrings: ({ fuzzy, limit } = {}) => rest.get('/string', { params: { fuzzy, limit } }),
  writeString: string => rest.put('/string', string),
  readString: id => rest.get(`/node/${encode(id)}/string`),

  // Node
  listNodeTypes: id => rest.get(`/node/${encode(id)}/types`),
  listHeads: id => rest.get(`/node/${encode(id)}/heads`),
  listTails: id => rest.get(`/node/${encode(id)}/tails`),

  // Names
  listNames: id => rest.get(`/node/${encode(id)}/names`),
  listStringNames: id => rest.get(`/node/${encode(id)}/string-names`),
  addName: (id, nameId) => rest.post(`/node/${encode(id)}/names`, nameId),

  // Set
  writeSet: set => rest.put('/set', set),
  readSet: id => rest.get('/set', id),

  // Map
  writeMap: map => rest.put('/map', map),

  // Audit
  listLabels: () => rest.get('/audit/label'),
  listLabelNodes: label => {
    const url = (label === undefined) ? '/audit/remainder' : `/audit/label/${label}`;
    return rest.get(url);
  },
  runAudit: () => rest.get('/audit'),

  // Map
  map: (id, inputs) => rest.post('/node/map', { id, inputs }),
});
