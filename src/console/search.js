import { useEffect } from 'react';
import { connect } from 'react-redux';

import { setNodes } from './store/actions';

export const Component = props => {
  const { entry, dispatch } = props;

  // Debounce
  useEffect(() => {
    const timeoutId = setTimeout(() => {
      if(!entry)
        return;

      dispatch(setNodes({ view: 'string-search' }));
    }, 300);
    return () => clearTimeout(timeoutId);
  });

  return null;
};

export const Search = connect(state => {
  const { entry } = state.console.nodes;
  return { entry };
})(Component);
