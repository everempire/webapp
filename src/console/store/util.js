export const setNode = (state, node, value) => {
  const nodes = { ...state.nodes };
  let { history } = state;

  if(value === undefined)
    delete nodes[node];
  else {
    nodes[node] = value;
    if(node in history) {
      // Update history
      let list = history[node];
      list = [...list, value];
      history = { ...history, [node]: list };
    }
  }

  return { ...state, nodes, history };
};
