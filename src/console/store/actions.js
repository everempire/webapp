// Node
export const setNodes = nodes => ({ type: 'SET_NODES', nodes });
export const setActiveNode = value => ({ type: 'SET_ACTIVE_NODE', value });
export const setFromActiveNode = node => ({ type: 'SET_FROM_ACTIVE_NODE', node });
// TODO - Append node?
// TODO - Backspace node?

// Action
export const addAction = input => ({ type: 'ADD_ACTION', ...input });
export const updateAction = (actionId, update) => ({ type: 'UPDATE_ACTION', actionId, update });
export const removeAction = actionId => ({ type: 'REMOVE_ACTION', actionId });

// History
export const removeHistoryItem = (name, index) => ({ type: 'REMOVE_HISTORY_ITEM', name, index });
export const clearHistory = name => ({ type: 'CLEAR_HISTORY', name });
