import { Reducer } from '../../store/redux-utils';

import { reducers as actionReducers } from '../action/reducers';

import { initialState } from './initial';
import { setNode } from './util';

export const reducer = Reducer({
  SET_NODES: (state, { nodes }) => {
    Object.entries(nodes).forEach(([node, value]) => {
      state = setNode(state, node, value);
    });
    return state;
  },
  SET_ACTIVE_NODE: (state, { value }) => {
    const { activeNode } = state.nodes;
    return setNode(state, activeNode, value);
  },
  SET_FROM_ACTIVE_NODE: (state, { node }) => {
    const { nodes } = state;
    const { activeNode } = nodes;

    return setNode(state, node, nodes[activeNode]);
  },

  ...actionReducers,

  REMOVE_HISTORY_ITEM: (state, { name, index }) => {
    let { history } = state;

    const oldHistory = history[name];
    const newHistory = [
      ...oldHistory.slice(0, index),
      ...oldHistory.slice(index + 1),
    ];

    history = { ...history, [name]: newHistory };

    return { ...state, history };
  },
  CLEAR_HISTORY: (state, { name }) => {
    let { history } = state;
    history = { ...history, [name]: [] };

    return { ...state, history };
  },
}, initialState);
