export const initialState = {
  nodes: {
    activeNode: 'primary',
    input: 'master',
    view: 'auto',

    showNodeActions: true,
  },

  nodeList: [
    'primary',
    ...['2', '3', '4', '5'],
    ...['6', '7', '8', '9', '0'],

    'activeNode',
    'input',
    'view',
    'selectMode',
  ],

  actions: [],

  history: {
    primary: [],
    2: [], 3: [], 4: [], 5: [],
    6: [], 7: [], 8: [], 9: [], 0: [],
  },
};
