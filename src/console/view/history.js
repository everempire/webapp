import classNames from 'classnames';
import React from 'react';
import { connect } from 'react-redux';

import { Close } from 'app/close';
import { Label, SimpleLabel } from 'app/label';
import { Node } from 'app/node';

import { setNodes, clearHistory, removeHistoryItem } from '../store/actions';

import { Style } from './history.style';

export const Component = ({ history, nodeList, historyViewSelection }) => {
  const keys = nodeList.filter(node => node in history && history[node].length);

  const selection = historyViewSelection || 'primary';
  const items = history[selection];

  return (
    <Style>
      <div className="lists">
        <div className="header">
          <span>Lists</span>
        </div>
        <div className="content">
          {keys.length === 0 && <span>No history</span>}
          {keys.map(key => {
            const className = classNames({ 'active-list': key === selection });
            const value = { dispatch: setNodes({ historyViewSelection: key }), value: key };

            return <SimpleLabel key={key} className={className} value={value}>{key}</SimpleLabel>;
          })}
        </div>
      </div>
      <div className="list">
        <div className="header">
          <span>Items</span>
          <SimpleLabel value={{ dispatch: clearHistory(selection) }}>Clear</SimpleLabel>
          <SimpleLabel value={{ action: { id: 'writeSet', input: items } }}>Write Set</SimpleLabel>
        </div>
        <div className="content">
          {items.map((value, index) => (
            // eslint-disable-next-line react/no-array-index-key
            <div key={index}>
              <Label value={{ value }}>
                <Node value={value}/>
              </Label>
              <Close value={{ dispatch: removeHistoryItem(selection, index) }}/>
            </div>
          ))}
        </div>
      </div>
    </Style>
  );
};

export const HistoryView = connect(state => {
  const { nodes, nodeList, history } = state.console;
  const { historyViewSelection } = nodes;
  return { history, nodeList, historyViewSelection };
})(Component);
