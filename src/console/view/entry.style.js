import styled from 'styled-components';

import { Label } from 'app/label';

export const Style = styled(Label)`
  display: flex;
  position: relative;

  align-items: center;

  font-family: monospace;
  font-weight: bold;

  padding: 4px;

  & > .tag {
    left: 0;
    right: initial;
  }

  &.active {
    border: 2px solid #ff8000;
  }

  pre.value {
    display: inline-block;

    margin: 0px 0px 0px 4px;
    padding: 2px 4px;
  }

  &.not-empty pre.value {
    color: white;
    background-color: #888;

    border-radius: 4px;
  }

  & > .label.register {
    position: absolute;
    left: -5px;
    top: -12px;
  }

  & > .label.value {
    margin-left: 4px;
  }
`;
