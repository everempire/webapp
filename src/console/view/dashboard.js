import { MASTER } from '@kitsune-system/common';

import React, { useEffect, useState } from 'react';
import styled from 'styled-components';

import { Label } from 'app/label';
import { Node } from 'app/node';
import { Rest } from 'rest';

const { listTails } = Rest.console;

const Style = styled.div`
  .label {
    margin-right: 4px;
  }
`;

export const Dashboard = () => {

  const [nodes, setNodes] = useState([]);

  useEffect(() => listTails(MASTER).then(setNodes), []);

  return (
    <Style className="dashboard">
      <h1>Dashboard</h1>

      <p>This is shown when `activeNode` is NULL.</p>
      <p>You can put a bunch of default actions and stuff here</p>

      <ul>
        {nodes.map(node => (
          <li key={node}>
            <Label value={{ value: node }}>
              <Node value={node}/>
            </Label>
          </li>
        ))}
      </ul>
    </Style>
  );
};
