import styled from 'styled-components';

export const Style = styled.div`
  &.array > .item {
    border: 2px solid red;
    margin-bottom: 8px;
    padding: 4px;
  }

  &.object .entry {
    display: flex;
    width: 100%;

    margin-bottom: 10px;

    .key,.value {
      flex-grow: 1;
    }

    &:last-child {
      margin-bottom: 0;
    }

    & > .key {
      margin-right: 8px;
    }
  }
`;
