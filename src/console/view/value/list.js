import React from 'react';
import styled from 'styled-components';

import { Label } from 'app/label';

const Style = styled.div`
  .list-view-item-view-node {
    margin-bottom: 4px;
  }

  & > .item {
    display: block;
    position: relative;

    padding: 4px;
    margin-bottom: 4px;

    & > .tag {
      top: -10px;
      left: -10px;
    }
  }
`;

export const ListView = ({ itemView, value }) => {
  const ItemView = itemView;

  return (
    <Style className="list-view">
      {/* TODO: Move this to an outer component */}
      {/*
      <div>
        <Label className="list-view-item-view-node" value={{ node: 'listViewItemView', value: listViewItemView }}>
          <Node value={listViewItemView}/>
        </Label>
        <b>&nbsp;listViewItemView</b>
      </div>
      */}
      {ItemView === undefined && <b>No itemView set</b>}
      {ItemView && value && value.map(item => {
        const { id } = item;
        return (
          <Label key={id} className="item" value={{ value: id }}>
            <ItemView value={item}/>
          </Label>
        );
      })}
    </Style>
  );
};
