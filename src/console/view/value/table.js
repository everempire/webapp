import React from 'react';
import styled from 'styled-components';

import { Label } from 'app/label';
import { Node } from 'app/node';

const Style = styled.table`
  table-layout: fixed;

  border-collapse: collapse;
  width: 100%;

  thead tr th {
    border-bottom: 2px solid grey;
    padding-bottom: 4px;

    input {
      font-weight: bold;
    }
  }

  tbody tr td {
    padding-top: 4px;
  }

  th > *, td > * {
    width: 100%;
  }
`;

export const TableView = ({ value }) => {
  if(!value)
    return null;

  const keys = value[0] ? Object.keys(value[0]) : [];

  return (
    <Style className="table-view">
      <thead>
        <tr>
          {keys.map(key => (
            <th key={key}>
              <Label value={{ value: key }}>
                <Node value={key}/>
              </Label>
            </th>
          ))}
        </tr>
      </thead>

      <tbody>
        {value && value.map && value.map((row, index) => (
          // eslint-disable-next-line react/no-array-index-key
          <tr key={index}>
            {keys.map(key => (
              <td key={key}>
                <Label value={{ value: row[key] }}>
                  <Node idOnly={key === 'id'} value={row[key]}/>
                </Label>
              </td>
            ))}
          </tr>
        ))}
      </tbody>
    </Style>
  );
};
