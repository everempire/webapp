import React from 'react';
import styled from 'styled-components';

import { Label } from 'app/label';
import { Node } from 'app/node';

const Style = styled.div`
  display: flex;

  & > .head, & > .tail {
    flex-grow: 1;
    flex-basis: 0;
  }

  & > .head {
    margin-right: 4px;
  }

  & > .tail {
    margin-left: 4px;
  }

  & > .id {
    padding: 0 4px;
  }
`;

export const Edge = ({ value }) => {
  const { id, head, tail } = value;

  return (
    <Style className="edge">
      {head && <Label className="head" value={{ value: head }}><Node value={head}/></Label>}
      <Label className="id" value={{ value: id }}><i className="fa fa-arrow-right"/></Label>
      {tail && <Label className="tail" value={{ value: tail }}><Node value={tail}/></Label>}
    </Style>
  );
};
