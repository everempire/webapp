import classNames from 'classnames';
import React from 'react';

import { Node } from '../../node';

import { Style } from './basic.style';

export const BasicView = ({ className, value }) => {
  if(typeof value === 'string')
    return <Style className={className}><Node value={value}/></Style>;

  if(Array.isArray(value)) {
    return (
      <Style className={classNames('basic-view', 'array', className)}>
        // eslint-disable-next-line react/no-array-index-key
        {value.map((subValue, index) => <BasicView key={index} className="item" value={subValue}/>)}
      </Style>
    );
  }

  if(typeof value === 'object') {
    return (
      <Style className={classNames('basic-view', 'object', className)}>
        {Object.entries(value).map(([key, value]) => {
          return (
            <div key={key} className="entry">
              <Node className="key" value={key}/>
              <BasicView className="value" value={value}/>
            </div>
          );
        })}
      </Style>
    );
  }

  return <Style className={className}><pre>{JSON.stringify(value, null, 2)}</pre></Style>;
};
