import React from 'react';

export const JsonView = ({ value }) => {
  return <pre>{JSON.stringify(value, null, 2)}</pre>;
};
