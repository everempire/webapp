import React from 'react';
import styled from 'styled-components';

import { Label } from 'app/label';

const Style = styled(Label)`
  padding: 0 2px;

  i {
    font-size: 16pt;
  }
`;

export const Close = props => (
  <Style className="close" {...props}>
    <i className="fa fa-close"/>
  </Style>
);
