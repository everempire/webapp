import { NODE, EDGE, SET, STRING } from '@kitsune-system/common';
import classNames from 'classnames';
import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';

import { Rest } from 'rest';
import { Label } from 'app/label';
import { Node } from 'app/node';

import { setNodes } from '../store/actions';

import { EdgeView } from './node/edge';
import { NodeView } from './node/node';
import { SetView } from './node/set';
import { StringView } from './node/string';

import { views as rootViews } from '.';
import { Dashboard } from './dashboard';

import { Style } from './auto.style';

const { listNodeTypes } = Rest.console;

const views = {
  [NODE]: NodeView,
  [EDGE]: EdgeView,
  [SET]: SetView,
  [STRING]: StringView,
};

const defaultModes = [NODE];

export const Component = ({ autoViewMode, value }) => {
  const [modes, setModes] = useState([]);

  useEffect(() => {
    // Don't use effect when `Dashboard` is showing
    if(value === undefined)
      return;

    // Reset component
    setModes([]);

    // TODO: Use `useNodeInfo` instead
    listNodeTypes(value).then(sets => {
      const modeList = [...defaultModes, ...sets];
      const modes = modeList.filter(mode => mode in views);
      modes.reverse();

      setModes(modes);
    });
  }, [value]);

  if(!value)
    return <Dashboard/>;

  if(value in rootViews) {
    const RootView = rootViews[value];
    return <RootView/>;
  }

  if(modes.indexOf(autoViewMode) === -1)
    autoViewMode = modes[0] || undefined;

  const View = views[autoViewMode] || (() => <div>No view for <b>{autoViewMode}</b></div>);

  return (
    <Style className="auto-view">
      <Node value={value}/>
      <div className="modes">
        {modes && modes.map(mode => {
          const value = { dispatch: setNodes({ autoViewMode: mode }), value: mode };
          return (
            <Label key={mode} className={classNames({ active: mode === autoViewMode })} value={value}>
              <Node value={mode}/>
            </Label>
          );
        })}
      </div>
      <View className="view" value={value}/>
    </Style>
  );
};

export const AutoView = connect(state => {
  const { nodes } = state.console;
  const { activeNode, autoViewMode, autoViewTarget } = nodes;

  // Get value via target
  const target = autoViewTarget || activeNode;
  const value = nodes[target];

  return { autoViewMode, value };
})(Component);
