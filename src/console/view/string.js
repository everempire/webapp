import React, { useEffect, useState } from 'react';

import { Rest } from 'rest';

import { TableView } from './value/table';

const { listStrings } = Rest.console;

export const StringView = ({ fuzzy, limit }) => {
  const [strings, setStrings] = useState([]);

  useEffect(() => {
    listStrings({ fuzzy, limit }).then(setStrings);
  }, [fuzzy, limit]);

  return <TableView value={strings}/>;
};
