import styled from 'styled-components';

export const Style = styled.div`
  display: flex;
  width: 100%;

  & > .lists {
    margin-right: 2px;

    & > .content > * {
      display: block;
    }

    & > .content > .label.active-list {
      border: 3px solid black;

      color: white;
      background-color: green;
    }
  }

  & > .list {
    margin-left: 2px;

    & > .content > * {
      display: flex;

      .label:first-child {
        margin-right: 4px;
        flex-grow: 1;
      }
    }
  }

  & > .lists, & > .list {
    flex-basis: 100%;
    flex-grow: 1;

    & > .header {
      height: 28px;

      & > * {
        margin-right: 4px;
      }

      span {
        font-size: large;
        font-weight: bold;
      }
    }

    & > .content > * {
      margin-bottom: 4px;
    }
  }
`;
