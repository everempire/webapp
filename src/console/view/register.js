import classNames from 'classnames';
import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';

import { Label } from 'app/label';
import { Node } from 'app/node';

const Style = styled.div`
  display: flex;
  margin-bottom: 4px;

  & > .label {
    display: inline-block;
    width: 120px;
  }

  & > .name {
    flex-grow: 1;
    margin-left: 4px;
    padding: 0 4px;
  }

  &.active > .name {
    color: white;
    background-color: black;
    border-radius: 4px;
    font-weight: bold;
  }
`;

export const RegisterComponent = ({ node, value, active }) => {
  return (
    <Style className={classNames('register', { active })}>
      <Label value={{ node, value }}>
        <Node value={value}/>
      </Label>
      <div className="name">{node}</div>
    </Style>
  );
};

export const Register = connect((state, ownProps) => {
  const { nodes } = state.console;
  const { autoViewTarget, view } = nodes;
  const { node } = ownProps;

  const active = view === 'auto' && node === autoViewTarget;
  const value = nodes[node];

  return { active, node, value };
})(RegisterComponent);
