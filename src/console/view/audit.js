import classNames from 'classnames';
import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';

import { Rest } from 'rest';
import { Label, SimpleLabel } from 'app/label';
import { Node } from 'app/node';

import { setNodes } from '../store/actions';

import { Style } from './audit.style';

const { listLabels, listLabelNodes, runAudit } = Rest.console;

export const AuditViewComponent = ({ auditViewLabel }) => {
  const [labels, setLabels] = useState([]);
  const [labelNodes, setLabelNodes] = useState([]);

  useEffect(() => {
    runAudit()
      .then(() => listLabels())
      .then(labels => setLabels(labels.sort()));
  }, []);

  useEffect(() => {
    if(labels.length === 0)
      return;

    listLabelNodes(auditViewLabel).then(setLabelNodes);
  }, [labels, auditViewLabel]);

  return (
    <Style className="audit-view">
      <h1>Audit</h1>

      <div className="columns">
        <div className="labels">
          <SimpleLabel
            className={classNames({ active: auditViewLabel === undefined })}
            value={{ dispatch: setNodes({ auditViewLabel: undefined }) }}
          >
            [REMAINDER]
          </SimpleLabel>

          {labels.map(label => (
            <SimpleLabel
              key={label}
              className={classNames({ active: auditViewLabel === label })}
              value={{ dispatch: setNodes({ auditViewLabel: label }), value: label }}
            >
              {label}
            </SimpleLabel>
          ))}
        </div>

        <div className="nodes">
          {labelNodes.map(node => (
            <Label key={node} value={{ value: node }}>
              <Node value={node}/>
            </Label>
          ))}
        </div>
      </div>
    </Style>
  );
};

export const Component = AuditViewComponent;

export const AuditView = connect(state => {
  const { auditViewLabel } = state.console.nodes;
  return { auditViewLabel };
})(AuditViewComponent);
