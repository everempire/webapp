import React from 'react';
import { connect } from 'react-redux';

import { AuditView } from './audit';
import { AutoView } from './auto';
import { GraphView } from './graph';
import { HistoryView } from './history';
import { StringView } from './string';
import { StringSearchView } from './string-search';

export const views = {
  audit: AuditView,
  auto: AutoView,
  graph: GraphView,
  history: HistoryView,
  string: StringView,
  'string-search': StringSearchView,
};

export const Component = ({ view }) => {
  const SubView = views[view] || views.auto;
  return <div className="view"><SubView/></div>;
};

export const View = connect(state => {
  const { view } = state.console.nodes;
  return { view };
})(Component);
