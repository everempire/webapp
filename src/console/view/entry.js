import classNames from 'classnames';
import React from 'react';
import { connect } from 'react-redux';

import { Style } from './entry.style';

export const Component = ({ entry, input }) => {
  let entryDisplay = entry || '';
  entryDisplay = entryDisplay.endsWith('\n') ? entryDisplay + ' ' : entryDisplay;

  const isEntryEmpty = entryDisplay.length === 0;
  const entryClasses = { active: input === 'entry', empty: isEntryEmpty, 'not-empty': !isEntryEmpty };

  return (
    <Style className={classNames('entry', entryClasses)} value={{ node: 'entry', value: entry }}>
      <span>Entry:</span>
      <pre className="value">{entryDisplay ? entryDisplay : 'None'}</pre>
    </Style>
  );
};

export const Entry = connect(state => {
  const { entry, input } = state.console.nodes;
  return { entry, input };
})(Component);
