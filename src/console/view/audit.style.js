import styled from 'styled-components';

export const Style = styled.div`
  & > .columns {
    display: flex;

    & > .labels {
      margin-right: 2px;

      .label {
        .children {
          white-space: nowrap;
        }

        &.active {
          color: white;
          background-color: black;
        }
      }
    }

    & > *:last-child {
      margin-left: 2px;
    }

    & > .nodes {
      flex-grow: 1;
    }

    & > *  > .label {
      display: block;
      margin-bottom: 4px;
    }
  }
`;
