import React, { useEffect, useState } from 'react';

import { Rest } from 'rest';

import { TableView } from './value/table';

const { listEdges } = Rest.console;

export const GraphView = () => {
  const [edges, setEdges] = useState([]);

  useEffect(() => {
    listEdges().then(edges => setEdges(edges));
  }, []);

  return <TableView value={edges}/>;
};
