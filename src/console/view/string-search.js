import React from 'react';
import { connect } from 'react-redux';

import { StringView } from './string';

export const Component = ({ entry }) => {
  return entry ? <StringView fuzzy={entry} limit={10}/> : null;
};

export const StringSearchView = connect(state => {
  const { entry } = state.console.nodes;
  return { entry };
})(Component);
