import styled from 'styled-components';

export const Style = styled.div`
  & > .node {
    border: 1px solid black;
  }

  ul {
    padding: 0;
    margin: 4px 0;
  }

  .modes {
    margin: 4px 0;

    & > .label {
      border: 4px solid black;
      border-radius: 4px;

      margin-right: 4px;

      font-weight: bold;
    }

    & > .label.active {
      border: 3px solid green;
      background-color: green;

      .node {
        color: white;
      }
    }
  }
`;
