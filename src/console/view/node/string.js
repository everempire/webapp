import React from 'react';

import { useNodeInfo } from 'app/node';

export const StringView = ({ value }) => {
  const { string } = useNodeInfo(value);
  return <pre>{string}</pre>;
};
