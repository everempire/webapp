import styled from 'styled-components';

export const Style = styled.div`
  display: flex;

  .heads {
    padding-right: 4px;
    border-right: 2px solid black;
  }

  .tails {
    padding-left: 4px;
    border-left: 2px solid black;
  }

  .heads,.tails {
    flex-grow: 1;
    flex-basis: 0;

    .header {
      font-size: large;
      font-weight: bold;
      text-align: center;
      margin-bottom: 4px;
    }

    & > .list > div {
      display: flex;
      margin-bottom: 4px;

      .label.edge {
        text-align: center;
        padding: 0 10px;
      }

     .label.primary {
       flex-grow: 1;
     }
    }

    .new {
      display: inline-block;
      padding: 0 5px;
      margin-left: 4px;
    }
  }

  .heads .label.edge {
    margin-left: 4px;
  }

  .tails .label.edge {
    margin-right: 4px;
  }
`;
