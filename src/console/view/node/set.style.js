import styled from 'styled-components';

export const Style = styled.div`
  .buttons {
    display: flex;

    width: 100%;
    margin-bottom: 4px;

    & > *:first-child {
      flex-grow: 1;
    }
  }

  .empty-msg {
    font-size: x-large;
    font-weight: bold;
  }

  .edge {
    padding-bottom: 4px;
  }
`;
