import { hashEdge } from '@kitsune-system/common';
import React, { useEffect, useState } from 'react';

import { Rest } from 'rest';

import { Label } from 'app/label';
import { Edge } from 'app/view';

import { addAction } from '../../store/actions';

import { Style } from './node.style';

const { listHeads, listTails } = Rest.console;

const addActionValue = (focus, initial, value) => ({
  dispatch: addAction({ actionId: 'writeEdge', focus, initial: { [initial]: value } }),
});

const AddAction = ({ focus, initial, value }) => (
  <Label className="new" value={addActionValue(focus, initial, value)}>
    <i className="fa fa-plus"/>
  </Label>
);

export const NodeView = ({ value }) => {
  const [heads, setHeads] = useState();
  const [tails, setTails] = useState();

  useEffect(() => {
    listHeads(value).then(setHeads);
    listTails(value).then(setTails);
  }, [value]);

  return (
    <Style>
      <div className="heads">
        <div className="header">
          Heads <AddAction focus="head" initial="tail" value={value}/>
        </div>
        <div className="list">
          {heads === undefined && <div>Loading</div>}
          {heads && heads.map(head => {
            const edgeNode = hashEdge([head, value]);
            return <Edge key={head} value={{ id: edgeNode, head }}/>;
          })}
        </div>
      </div>
      <div className="tails">
        <div className="header">
          Tails <AddAction focus="tail" initial="head" value={value}/>
        </div>
        <div className="list">
          {tails === undefined && <div>Loading</div>}
          {tails && tails.map(tail => {
            const edgeNode = hashEdge([value, tail]);
            return <Edge key={tail} value={{ id: edgeNode, tail }}/>;
          })}
        </div>
      </div>
    </Style>
  );
};
