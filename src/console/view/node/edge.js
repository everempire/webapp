import React, { useEffect, useState } from 'react';
import styled from 'styled-components';

import { Rest } from 'rest';
import { SimpleLabel } from 'app/label';
import { Edge } from 'app/view';

const { readEdge } = Rest.console;

const Style = styled.div`
  & > .actions {
    display: flex;

    & > *:first-child {
      flex-grow: 1;
    }
  }

  & > .edge {
    margin-top: 4px;
  }
`;

export const EdgeView = ({ value }) => {
  const [edge, setEdge] = useState({});

  useEffect(() => {
    readEdge(value).then(setEdge);
  }, [value]);

  return (
    <Style>
      <div className="actions">
        <div/>
        <SimpleLabel value={{ action: { id: 'destroyEdge', input: value } }}>
          Destroy
        </SimpleLabel>
      </div>
      <Edge value={edge}/>
    </Style>
  );
};
