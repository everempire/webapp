import { hashEdge } from '@kitsune-system/common';
import React, { useEffect, useState } from 'react';

import { Rest } from 'rest';
import { SimpleLabel } from 'app/label';
import { Edge } from 'app/view';

import { addAction } from '../../store/actions';

import { Style } from './set.style';

const { listTails } = Rest.console;

export const SetView = ({ value }) => {
  const [set, setSet] = useState([]);

  useEffect(() => {
    listTails(value).then(setSet);
  }, [value]);

  const addValue = {
    dispatch: addAction({
      actionId: 'writeEdge',
      focus: 'tail',
      initial: { head: value },
    }),
  };

  const initial = {};
  set.forEach(node => (initial[node] = undefined));

  const mapValue = {
    dispatch: addAction({
      actionId: 'writeMap',
      initial,
    }),
  };

  return (
    <Style>
      <div className="buttons">
        <div><SimpleLabel value={addValue}>Add</SimpleLabel></div>
        <div><SimpleLabel value={mapValue}>Build Map</SimpleLabel></div>
      </div>

      {set.length === 0 && <div className="empty-msg">Empty Set</div>}
      {set.map(tail => {
        const edgeNode = hashEdge([value, tail]);
        return <Edge key={tail} value={{ id: edgeNode, tail }}/>;
      })}
    </Style>
  );
};
