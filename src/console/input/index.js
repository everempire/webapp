import { on } from '@gamedevfox/katana';
import { useEffect } from 'react';
import { connect } from 'react-redux';

import { useLabelControls } from 'app/label';

import { bindActions } from '../../store/redux-utils';

import * as actions from '../store/actions';

import { input as root } from './root';
import { input as bind } from './modes/bind';
import { input as entry } from './modes/entry';
import { input as entryRegister } from './modes/entry-register';
import { input as master } from './modes/master';
import { input as select } from './modes/select';
import { input as shift } from './modes/shift';

export const entryKey = 'Enter';
export const selectKey = 'Space';

const inputs = {
  master,

  bind,
  entry,
  'entry-register': entryRegister,
  select,
  shift,
};

export const Component = ({ state, dispatch }) => {
  const boundActions = bindActions(actions, dispatch);

  const labelControls = useLabelControls();

  useEffect(() => {
    const args = { actions: boundActions, inputs, state, labelControls, dispatch };

    const offs = Object.entries(root)
      .map(([name, fn]) => on(window, name, event => fn({ ...args, event })));

    return () => offs.forEach(off => off());
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [state]);

  return null;
};

export const Input = connect(state => ({ state: state.console }))(Component);
