import { Rest } from 'rest';
import { setActiveNode } from '../store/actions';

const { random } = Rest;
const { destroyEdge, writeSet } = Rest.console;

export const actions = {
  random: ({ dispatch }) => {
    random().then(id => dispatch(setActiveNode(id)));
  },

  writeSet: ({ input: set, dispatch }) => {
    writeSet(set).then(id => dispatch(setActiveNode(id)));
  },

  destroyEdge: ({ input: edgeId, dispatch }) => {
    destroyEdge(edgeId).then(() => dispatch(setActiveNode(undefined)));
  },
};
