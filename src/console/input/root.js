import _ from 'lodash';

const chooseInput = (inputs, state) => {
  const { input } = state.nodes;
  return inputs[input] || {};
};

const onKeyDown = args => {
  const { actions, event, inputs, state, labelControls } = args;
  const { code } = event;
  const { clear } = labelControls;

  if(code === 'Escape') {
    const { setNodes } = actions;
    clear();
    setNodes({
      // activeNode: 'primary',
      // autoViewTarget: 'primary',
      input: 'master',
      view: 'auto', showNodeActions: true,
    });
    return;
  }

  const input = chooseInput(inputs, state);
  if('keydown' in input)
    input.keydown(_.omit(args, 'inputs'));
};

const onKeyUp = args => {
  const { inputs, state } = args;

  const input = chooseInput(inputs, state);
  if('keyup' in input)
    input.keyup(_.omit(args, 'inputs'));
};

export const input = { keydown: onKeyDown, keyup: onKeyUp };
