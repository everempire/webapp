import { setNodes } from '../../store/actions';

import { entryKey } from '..';

export const input = {
  keydown: ({ event, state, labelControls, dispatch }) => {
    const { code } = event;
    const { nodeList } = state;
    const { clear } = labelControls;

    if(code.startsWith('Digit')) {
      let digit = parseInt(code.slice(5).toLowerCase(), 10);
      if(digit === 0)
        digit = 10;

      const nodeId = nodeList[digit - 1];
      dispatch(setNodes({ autoViewTarget: nodeId, view: 'auto' }));
      return;
    }

    if(code === entryKey) {
      clear();
      dispatch(setNodes({ input: 'entry' }));
    }

    if(code === 'Backquote') {
      const { idOnly } = state.nodes;
      dispatch(setNodes({ idOnly: !(idOnly === true) }));
    }
  },

  keyup: ({ event, labelControls, dispatch }) => {
    const { code } = event;
    const { clear } = labelControls;

    if(code === 'ShiftLeft') {
      clear();
      dispatch(setNodes({ input: 'master' }));
    }
  },
};
