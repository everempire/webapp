import { setNodes } from '../../store/actions';

import { entryKey, selectKey } from '..';
import { actions } from '../actions';

export const input = {
  keydown: ({ event, state, labelControls, dispatch }) => {
    const { code } = event;
    const { nodes, nodeList } = state;
    const { mark } = labelControls;

    if(code === 'Backquote') {
      const { autoViewTarget } = nodes;
      const newTarget = autoViewTarget === undefined ? 'primary' : undefined;

      dispatch(setNodes({ autoViewTarget: newTarget, view: 'auto' }));
      return;
    }

    if(code === 'Tab') {
      event.preventDefault();

      const { showNodeActions } = nodes;
      dispatch(setNodes({ showNodeActions: !showNodeActions }));
    }

    if(code.startsWith('Digit')) {
      let digit = parseInt(code.slice(5).toLowerCase(), 10);
      if(digit === 0)
        digit = 10;

      const nodeId = nodeList[digit - 1];
      dispatch(setNodes({ activeNode: nodeId }));
      return;
    }

    if(code === selectKey) {
      event.preventDefault();

      mark();
      dispatch(setNodes({ input: 'select' }));
      return;
    }

    if(code === 'ShiftLeft') {
      dispatch(setNodes({ input: 'shift' }));
      return;
    }

    if(code === 'ShiftRight') {
      dispatch(setNodes({ input: 'bind' }));
      return;
    }

    if(code === entryKey)
      dispatch(setNodes({ entry: undefined, input: 'entry-register' }));

    // Check bindings
    if(code in nodes) {
      const id = nodes[code];

      if(!(id in actions))
        throw new Error(`No such action with id: ${id}`);

      const action = actions[id];
      action({ dispatch });
    }
  },
};
