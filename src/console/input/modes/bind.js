import { setNodes, setFromActiveNode } from '../../store/actions';

export const input = {
  keydown: ({ event, dispatch }) => {
    const { code } = event;
    dispatch(setFromActiveNode(code));
  },

  keyup: ({ event, dispatch }) => {
    if(event.code === 'ShiftRight')
      dispatch(setNodes({ input: 'master' }));
  },
};
