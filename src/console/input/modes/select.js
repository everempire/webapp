import _ from 'lodash';

import { setNodes } from '../../store/actions';
import { executeSelection } from '../../select';

import { selectKey } from '..';

const selectModes = {
  ArrowDown: {
    mode: 'value',
    markFn: value => value?.value,
  },
  ArrowLeft: {
    mode: 'node',
    markFn: value => value?.node,
  },
  ArrowRight: {
    mode: 'setNode',
    markFn: value => value?.node,
  },
};

export const input = {
  keydown: args => {
    const { event, state, labelControls, dispatch } = args;
    const { code } = event;
    const { nodes } = state;
    const { select, mark } = labelControls;

    const label = nodes.label || '';

    if(code in selectModes) {
      const { mode, markFn } = selectModes[code];
      const { label } = nodes;

      const oldSelect = select(label);
      mark(markFn);
      const newSelect = select(label);

      const newNodes = { selectMode: mode };
      if(!_.isEqual(oldSelect, newSelect))
        newNodes.label = undefined;

      dispatch(setNodes(newNodes));
      return;
    }

    if(code.startsWith('Key')) {
      const key = code.slice(3).toLowerCase();
      dispatch(setNodes({ label: label + key }));
    } else if(code === 'Backspace')
      dispatch(setNodes({ label: label.slice(0, label.length - 1) }));
  },

  keyup: args => {
    const { event, state, labelControls, dispatch } = args;
    const { code } = event;
    const { nodes } = state;
    const { select, clear } = labelControls;

    const { label, selectMode } = nodes;

    if(code === selectKey) {
      if(label) {
        const selection = select(label);
        executeSelection({ args, mode: selectMode, selection, dispatch });
      }

      clear();
      dispatch(setNodes({
        label: undefined, selectMode: undefined, input: 'master',
      }));
    }
  },
};
