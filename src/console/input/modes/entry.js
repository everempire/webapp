import { entryKey } from '..';

export const input = {
  keydown: ({ event, state, actions }) => {
    const { code, key } = event;
    const { nodes } = state;
    const { setNodes, setActiveNode } = actions;

    const entry = nodes.entry || '';

    if(code === entryKey) {
      if(event.shiftKey)
        setNodes({ entry: entry + '\n' });
      else {
        setActiveNode(entry);
        setNodes({ input: 'master' });
      }
    } else if(code === 'Backspace')
      setNodes({ entry: entry.slice(0, entry.length - 1) });
    else if(key.length === 1)
      setNodes({ entry: entry + key });
  },
};
