import { entryKey } from '..';

export const input = {
  keydown: ({ event, state, actions }) => {
    const { code } = event;
    const { nodeList } = state;
    const { setNodes } = actions;

    if(code.startsWith('Digit')) {
      let digit = parseInt(code.slice(5).toLowerCase(), 10);
      if(digit === 0)
        digit = 10;

      const nodeId = nodeList[digit - 1];
      setNodes({ activeNode: nodeId });
    }
  },
  keyup: ({ event, actions }) => {
    const { code } = event;
    const { setNodes } = actions;

    if(code === entryKey)
      setNodes({ input: 'entry' });
  },
};
