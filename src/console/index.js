import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';

import { SimpleLabel as Label } from 'app/label';

import { actions as actionFns } from './action';
import { ActionList } from './action/action-list';
import { ActionListener } from './action/listener';

import { setNodes, addAction } from './store/actions';
import { Input } from './input';
import { Search } from './search';

import { View } from './view';
import { Entry } from './view/entry';
import { Register } from './view/register';

import { Style } from './index.style';

const LabelView = styled.div`
  display: inline-block;

  position: fixed;
  top: 8px;
  right: 8px;

  color: white;
  background-color: black;

  border-radius: 4px;
  padding: 1px 4px;

  font-family: monospace;
  font-weight: bold;
`;

const views = ['history', 'audit', 'graph', 'string'];

export const Component = ({ nodeList, nodes }) => {
  const { label, showNodeActions } = nodes;

  return (
    <Style className="console">
      <ActionListener/>
      <Input/>
      <Search/>

      <h1>Console</h1>

      <div className="action-nodes">
        <div className="left">
          {views.map(view => (
            <Label key={view} value={{ dispatch: setNodes({ view }), value: view }}>
              {view}
            </Label>
          ))}
        </div>

        <div className="right">
          <Label value={{ action: 'random', value: 'random' }}>random</Label>
          {Object.keys(actionFns).map(actionId => {
            // eslint-disable-next-line react/no-array-index-key
            const value = {
              dispatch: addAction({ actionId }),
              value: actionId,
            };
            return (
              <Label key={actionId} value={value}>
                {actionId}
              </Label>
            );
          })}
        </div>
      </div>

      <Entry/>

      <div className="split">
        <div className="control">
          {nodeList.map(node => <Register key={node} node={node}/>)}
        </div>

        <div className="view-box">
          <View/>
        </div>
      </div>

      {label && label.length !== 0 && <LabelView className="label-view">{label}</LabelView>}

      {showNodeActions && <ActionList/>}
    </Style>
  );
};

export const Console = connect(state => {
  const { nodeList, nodes } = state.console;
  return { nodeList, nodes };
})(Component);
