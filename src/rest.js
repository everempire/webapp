// @flow
import EventEmitter from 'events';

import axios, { CancelToken } from 'axios';

import { endpoints as consoleEndpoints } from './console/rest';

const endpoints = rest => ({
  auth: ({ email }) => rest.post('/auth', { email }),

  listUsers: () => rest.get('/user'),
  register: ({ email, name }/*: {| email: string, name: string |} */) => {
    return rest.post('/user', { email, name });
  },

  getMe: () => rest.get('/me'),

  random: () => rest.post('/random'),
});

// TODO: Turn this in to component or set token in state?
export const build = ({ baseURL }) => {
  let token;
  const tokenEvent = new EventEmitter();

  const rest = axios.create({ baseURL });

  rest.interceptors.request.use(config => {
    const { headers, data } = config;

    if(token)
      headers.Authorization = `Basic ${token}`;

    headers['Content-Type'] = 'application/json';
    if(typeof data !== 'object')
      config.data = JSON.stringify(data);

    return config;
  });
  rest.interceptors.response.use(res => res.data);

  const setToken = newToken => {
    token = newToken;
    tokenEvent.emit('token', token);
  };

  const onToken = fn => {
    tokenEvent.on('token', fn);
    return () => tokenEvent.off('token', fn);
  };

  const withCancel = fn => (...args) => {
    const source = CancelToken.source();
    const { token } = source;

    const cancelFn = fn(token);
    const promise = cancelFn(...args);
    promise.cancel = source.cancel;

    return promise;
  };

  const twoArgs = ['get', 'delete', 'head', 'options'].map(name => {
    return [name, withCancel(cancelToken => (url, config = {}) => rest[name](url, { ...config, cancelToken }))];
  });
  const threeArgs = ['post', 'put', 'patch'].map(name => {
    return [name, withCancel(cancelToken => (url, data, config = {}) => rest[name](url, data, { ...config, cancelToken }))];
  });

  const cancelRest = {
    request: withCancel(cancelToken => config => rest.request({ ...config, cancelToken })),
    ...Object.fromEntries(twoArgs),
    ...Object.fromEntries(threeArgs),
  };

  return {
    setToken, onToken,

    ...endpoints(cancelRest),
    console: consoleEndpoints(cancelRest),
  };
};

export const Rest = build({ baseURL: 'http://localhost:8080' });
