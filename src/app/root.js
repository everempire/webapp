import React from 'react';
import { Provider } from 'react-redux';

import { store } from 'store';

import { Router } from './router';

import { GlobalStyle } from './root.style';

export const Root = () => (
  <Provider store={store}>
    <GlobalStyle/>
    <Router/>
  </Provider>
);
