import { modSplit } from '@gamedevfox/katana';

export const modSplitSpecial = base => {
  const split = modSplit(base);

  return num => {
    let order = 1;
    while(num >= base ** order) {
      num -= base ** order;
      order++;
    }

    const parts = split(num);

    while(parts.length < order)
      parts.unshift(0);

    return parts;
  };
};

const letters = 'asdqwe'.split('');
const split = modSplitSpecial(letters.length);

export const qweasd = num => {
  const parts = split(num);

  const result = parts.map(part => letters[part]);

  return result.join('');
};
