import styled from 'styled-components';

export const Style = styled.div`
  .nav {
    display: flex;

    width: 100%;

    color: white;
    background-color: black;
    padding: 4px;

    & > * {
      width: 80px;
    }
  }

  .sign-out {
    border: none;
    background: none;
    color: white;
    cursor: pointer;
    align-self: end;
  }
`;
