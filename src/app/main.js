import React, { useState } from 'react';
import { Link, Route } from 'react-router-dom';

import { Rest } from 'rest';

import { Style } from './main.style';

export const Main = () => {
  const [me, setMe] = useState();

  const onGetMeClick = () => {
    Rest.getMe().then(me => setMe(me));
  };

  return (
    <Style>
      <div className="nav">
        <Link to="/">Home</Link>
        <Link to="/one">One</Link>
        <Link to="/two">Two</Link>
        <button className="sign-out" onClick={() => Rest.setToken(undefined)}>Sign Out</button>
      </div>

      <div>
        <Route exact path="/">
          <h1>Welcome</h1>
          {me && <pre>{JSON.stringify(me, null, 2)}</pre>}
          <button type="button" onClick={onGetMeClick}>Get Me</button>
        </Route>
        <Route path="/one">First One</Route>
        <Route path="/two">Another One</Route>
      </div>

      Always
    </Style>
  );
};
