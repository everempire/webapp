export { useLabel, useLabelControls } from 'label/hooks';
export { Label, SimpleLabel, typeOf } from 'label/label';
export { Provider } from 'label/context';
