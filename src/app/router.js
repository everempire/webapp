import React, { useState, useEffect } from 'react';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';

import { Rest } from 'rest';
import { Provider as LabelProvider } from 'app/label';

import { SignIn } from '../auth/sign-in';
import { Console } from '../console';

import { Main } from './main';
import { qweasd } from './qweasd';

export const Router = () => {
  const [token, setToken] = useState();

  useEffect(() => Rest.onToken(setToken), []);

  return (
    <BrowserRouter>
      <Switch>
        <Route path="/console">
          <LabelProvider onLabel={qweasd}>
            <Console/>
          </LabelProvider>
        </Route>
        <Route path="/auth">
          {token && <Redirect to="/"/>}
          <SignIn/>
        </Route>
        <Route>
          {token === undefined && <Redirect to="/auth"/>}
          <Main/>
        </Route>
      </Switch>
    </BrowserRouter>
  );
};
