import { useEffect, useLayoutEffect, useState } from 'react';

import * as infoModuleBuilders from './node-info.modules.js';

export const NodeInfo = () => {
  let setInfoIdCounter = 0;

  const nodeIdInfoMap = {};
  const setInfoFnMap = {};

  const getInfo = id => nodeIdInfoMap[id] || {};

  const update = (id, data) => {
    const info = nodeIdInfoMap[id];
    const newInfo = { ...info, ...data };
    nodeIdInfoMap[id] = newInfo;

    const setInfos = setInfoFnMap[id];
    Object.values(setInfos).forEach(setInfo => setInfo(newInfo));
  };

  const infoModules = {};
  Object.entries(infoModuleBuilders).forEach(([name, builder]) => {
    infoModules[name] = builder({ nodeIdInfoMap, update });
  });

  const useNodeInfo = id => {
    const info = getInfo(id);
    const [, setInfo] = useState(info);

    useLayoutEffect(() => {
      const setInfoId = ++setInfoIdCounter;

      let subMap = setInfoFnMap[id];
      if(!subMap) {
        subMap = {};
        setInfoFnMap[id] = subMap;
      }

      subMap[setInfoId] = setInfo;

      return () => delete setInfoFnMap[id][setInfoId];
    }, [id, setInfo]);

    useEffect(() => {
      if(typeof id !== 'string' || id.length === 0)
        return () => {};

      // Run info modules
      Object.values(infoModules).forEach(module => module({ id }));
    }, [id, setInfo]);

    return info;
  };

  return { useNodeInfo };
};

export const { useNodeInfo } = NodeInfo();
