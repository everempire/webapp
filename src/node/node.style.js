import styled from 'styled-components';

export const Style = styled.div`
  display: flex;
  position: relative;

  & > .value {
    flex-grow: 1;
    border: 0;
    font-family: monospace;
    padding: 0 2px;
    overflow: hidden;

    & > * {
      width: 100%;
    }
  }

  &.edge > .value {
    display: flex;

    & > .label {
      flex-grow: 1;
      display: inline-block;
      flex-basis: 100%;
    }

    & > i {
      margin: 4px 4px 0 4px;
    }
  }

  & > button {
    border: 0;
    border-left: 1px solid grey;

    cursor: pointer;
  }
`;
