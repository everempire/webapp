import classNames from 'classnames';
import React from 'react';
import { connect } from 'react-redux';

import { useNodeInfo } from 'app/node';
import { Edge } from 'app/view';

import { Style } from './node.style';

const NodeComponent = ({ className, value, idOnly }) => {
  const onClick = e => {
    e.stopPropagation();

    const el = document.createElement('input');

    el.value = value;

    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
  };

  const classes = {
    boolean: typeof value === 'boolean',
    number: typeof value === 'number',
  };
  let render = value;

  if(value === undefined) {
    classes.null = true;
    render = 'NULL';
  }

  if(classes.boolean)
    render = JSON.stringify(value);

  const { edge, string, name } = useNodeInfo(value);

  if(!idOnly) {
    if(edge) {
      // TODO: Implement depth limit here
      classes.edge = true;
      render = <Edge value={edge}/>;
    }

    if(name) {
      classes.name = true;
      render = name;
    }

    if(string) {
      classes.string = true;
      render = string;
    }
  }

  return (
    <Style className={classNames('node', classes, className)}>
      <span className="value">{render}</span>
      <button type="button" onClick={onClick}>
        <i className="fa fa-clipboard"/>
      </button>
    </Style>
  );
};

export const Component = NodeComponent;

export const Node = connect((state, ownProps) => {
  let { idOnly } = state.console.nodes;
  idOnly = idOnly || ownProps.idOnly;
  return { idOnly };
})(NodeComponent);
