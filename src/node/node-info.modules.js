import _ from 'lodash';

import { Rest } from 'rest';

const { map } = Rest.console;

const isNonEmptyString = str => {
  return (
    str !== undefined &&
    typeof str === 'string' &&
    str.length !== 0
  );
};

const batchRequest = (restEndpoint, updateFn) => {
  const ids = new Set();

  const request = _.debounce(() => {
    const idList = Array.from(ids);
    map(restEndpoint, idList).then(results => {
      idList.forEach((id, index) => {
        const result = results[index];
        updateFn(id, result);
      });
    });
  });

  return id => {
    ids.add(id);
    request();
  };
};

export const edge = ({ nodeIdInfoMap, update }) => {
  const request = batchRequest('readEdge', (id, edge) => {
    update(id, { edge });
  });

  return ({ id }) => {
    const info = nodeIdInfoMap[id] || {};
    if(!isNonEmptyString(id) || 'edge' in info)
      return;

    request(id);
  };
};

export const string = ({ nodeIdInfoMap, update }) => {
  const request = batchRequest('readString', (id, string) => {
    update(id, { string });
  });

  return ({ id }) => {
    const info = nodeIdInfoMap[id] || {};
    if(!isNonEmptyString(id) || 'string' in info)
      return;

    request(id);
  };
};

export const name = ({ nodeIdInfoMap, update }) => {
  const request = batchRequest('getStringNames', (id, names) => {
    update(id, { name: names[0], names });
  });

  return ({ id }) => {
    const info = nodeIdInfoMap[id] || {};
    if(!isNonEmptyString(id) || 'name' in info)
      return;

    request(id);
  };
};
